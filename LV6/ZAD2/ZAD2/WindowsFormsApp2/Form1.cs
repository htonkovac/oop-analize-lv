﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {

        String Word;
        int Attempts;

        public Form1()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            List<string> Lines = new List<string>();

            using (System.IO.StreamReader reader = new System.IO.StreamReader("word.txt"))
            {
                string line;
                line = reader.ReadLine();
                string[] parts = line.Split(',');
                foreach (string s in parts)
                    Lines.Add(s);
            }
            Random rnd = new Random();
            int n = rnd.Next(0, Lines.Count - 1);
            Word = Lines[n];
            MessageBox.Show(Word);
        }
        private void play(char guess)
        {
            int counter = 0;
            foreach(char letter in Word)
            {
                counter++;
                if(letter == guess)
                {
                    MessageBox.Show($"The letter {letter} shows up in the word on the {counter}. spot");
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            play('a');
        }

        private void button3_Click(object sender, EventArgs e)
        {
            play('c');

        }

        private void button9_Click(object sender, EventArgs e)
        {
            play('i');

        }

        private void button2_Click(object sender, EventArgs e)
        {
            play('b');
        }

        private void button4_Click(object sender, EventArgs e)
        {
            play('d');

        }

        private void button5_Click(object sender, EventArgs e)
        {
            play('e');

        }

        private void button6_Click(object sender, EventArgs e)
        {
            play('f');

        }

        private void button7_Click(object sender, EventArgs e)
        {
            play('g');

        }

        private void button8_Click(object sender, EventArgs e)
        {
            play('h');

        }

        private void button10_Click(object sender, EventArgs e)
        {
            play('j');
        }

        private void button11_Click(object sender, EventArgs e)
        {
            play('k');

        }

        private void button12_Click(object sender, EventArgs e)
        {
            play('l');
        }

        private void button13_Click(object sender, EventArgs e)
        {
            play('m');

        }

        private void button14_Click(object sender, EventArgs e)
        {
            play('n');

        }

        private void button15_Click(object sender, EventArgs e)
        {
            play('o');

        }

        private void button16_Click(object sender, EventArgs e)
        {
            play('p');

        }

        private void button17_Click(object sender, EventArgs e)
        {
            play('r');

        }

        private void button18_Click(object sender, EventArgs e)
        {
            play('s');

        }

        private void button20_Click(object sender, EventArgs e)
        {
            play('t');

        }

        private void button19_Click(object sender, EventArgs e)
        {
            play('u');

        }

        private void button21_Click(object sender, EventArgs e)
        {
            play('v');

        }

        private void button22_Click(object sender, EventArgs e)
        {
            play('z');

        }

        private void button23_Click(object sender, EventArgs e)
        {
            play('x');

        }

        private void button24_Click(object sender, EventArgs e)
        {
            play('y');

        }

        private void button25_Click(object sender, EventArgs e)
        {
            play('z');

        }
    }
}

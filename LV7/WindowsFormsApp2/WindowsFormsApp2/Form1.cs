﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Graphics g1, g2, g3, g4, g5, g6, g7, g8, g9;

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            draw(g1);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            draw(g2);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            draw(g3);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            draw(g4);
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            draw(g5);
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            draw(g6);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            draw(g7);
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            draw(g8);
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            draw(g9);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        Pen pen = new Pen(Color.Blue, 10);
        bool isPlayerOneTurn;

        public Form1()
        {
            InitializeComponent();
        }

        private void draw(Graphics gr)
        {
            if (isPlayerOneTurn == true)
            {
                label3.Text = "Na potezu je " + textBox1.Text;
                gr.DrawEllipse(pen, 25,25, 30, 30);
                isPlayerOneTurn = !isPlayerOneTurn;
            }
            else
            {
                label3.Text = "Na potezu je " + textBox2.Text;
                gr.DrawLine(pen, 20, 20, 60, 60);
                gr.DrawLine(pen, 20, 60, 60, 20);
                isPlayerOneTurn = !isPlayerOneTurn;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            g1 = pictureBox1.CreateGraphics();
            g2 = pictureBox2.CreateGraphics();
            g3 = pictureBox3.CreateGraphics();
            g4 = pictureBox4.CreateGraphics();
            g5 = pictureBox5.CreateGraphics();
            g6 = pictureBox6.CreateGraphics();
            g7 = pictureBox7.CreateGraphics();
            g8 = pictureBox8.CreateGraphics();
            g9 = pictureBox9.CreateGraphics();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {


        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
